package com.neihanduanzi.api;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Administrator on 2016/10/31 0031.
 */

public interface DuanZiServer {

    @GET("stream/mix/v1/?content_type=-102")
    Call<String> getDuanZiList();

}
