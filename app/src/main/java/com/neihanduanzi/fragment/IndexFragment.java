package com.neihanduanzi.fragment;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.neihanduanzi.R;
import com.neihanduanzi.adapter.CommonFragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class IndexFragment extends BaseFragment {


    public IndexFragment() {
        // Required empty public constructor
    }

    @Override
    public String getFragmentTitle() {
        return "首页";
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View ret = inflater.inflate(R.layout.fragment_index, container, false);
        ViewPager viewPager = (ViewPager) ret.findViewById(R.id.index_viewpager);
        TabLayout tabLayout = (TabLayout) ret.findViewById(R.id.index_tablayout);

        List<BaseFragment> fragments = new ArrayList<>();
        fragments.add(new ZTuijianFragment());
        fragments.add(new ZVideoFragment());
        fragments.add(new ZImageFragment());
        fragments.add(new ZDuanziFragment());
        CommonFragmentPagerAdapter adapter = new CommonFragmentPagerAdapter(
                getChildFragmentManager(),
                fragments);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(6);
        tabLayout.setupWithViewPager(viewPager);
        return ret;
    }

}
