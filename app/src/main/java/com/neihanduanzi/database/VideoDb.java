package com.neihanduanzi.database;

import org.xutils.db.annotation.Column;
import org.xutils.db.annotation.Table;

/**
 * Created by 凸显 on 2016/11/4.
 */
@Table(name = "video")
public class VideoDb {
    @Column(name = "id",isId = true,autoGen = true)
    private int id;
    @Column(name = "group")
    private String group;

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
