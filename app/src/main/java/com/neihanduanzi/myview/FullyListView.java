package com.neihanduanzi.myview;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ListView;

/**
 * Created by vhly[FR].
 * <p>
 * Author: vhly[FR]
 * Email: vhly@163.com
 * Date: 16/9/27
 */

public class FullyListView extends ListView {
    public FullyListView(Context context) {
        super(context);
    }

    public FullyListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * 是一个控件自己来计算自身宽度、高度的;
     * 参数 是父容器指定的 最大可用宽度、高度,
     *                  或者是明确的指定当前控件的宽度、高度
     * @param widthMeasureSpec
     * @param heightMeasureSpec
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        // 获取高度的规则,以及高度的可用数值
        int mode = MeasureSpec.getMode(heightMeasureSpec);// 获取高度规则
        int height = MeasureSpec.getSize(heightMeasureSpec);

        Log.d("FullyListView", "height = " + height);

        switch (mode){
            case MeasureSpec.EXACTLY:
                Log.d("FullyListView", "mode = EXACTLY");
                break;
            case MeasureSpec.AT_MOST:
                Log.d("FullyListView", "mode = AT_MOST");
                break;
            case MeasureSpec.UNSPECIFIED:
                Log.d("FullyListView", "mode = UNSPECIFIED");
                break;
        }

        // 重新设置 高度的模式,设置为 AT_MOST, 实际测量所有Item的高度
        // 没有了视图的复用, 尽量不要使用图片 !!!!

        heightMeasureSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
