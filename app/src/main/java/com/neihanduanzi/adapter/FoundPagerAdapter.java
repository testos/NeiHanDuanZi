package com.neihanduanzi.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

/**
 * Created by Administrator on 2016/11/1 0001.
 */

public class FoundPagerAdapter extends PagerAdapter {
    private Context mContext;
    private List<ImageView> mImgs;

    public FoundPagerAdapter(Context context, List<ImageView> imgs) {
        mContext = context;
        mImgs = imgs;
    }


    @Override
    public int getCount() {
        return 300;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        container.addView(mImgs.get(position % (mImgs.size())));
        return mImgs.get(position % (mImgs.size()));
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
