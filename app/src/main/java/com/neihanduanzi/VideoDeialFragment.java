package com.neihanduanzi;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.neihanduanzi.model.GroupId;
import com.neihanduanzi.model.VideoModel;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;

import static com.neihanduanzi.adapter.VideoListAdapter.mUrl;

public class VideoDeialFragment extends Fragment implements  MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener, SurfaceHolder.Callback, View.OnClickListener {

    private MediaPlayer sMediaPlayer;
    private ImageView btnplay;
    private SurfaceView mSurfaceView;
    private ImageView Cover;
    private boolean isPlaying = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View ret = inflater.inflate(R.layout.activity_video_deial, container, false);
//        setContentView(R.layout.activity_video_deial);
        //接收Json
        String detial = getActivity().getIntent().getStringExtra("Data");
        Gson gson = new Gson();
        final VideoModel.Fdata.VideoDetial fdata = gson.fromJson(detial, VideoModel.Fdata.VideoDetial.class);
        //设置文字头像信息
        TextView username = ((TextView) ret.findViewById(R.id.user_name));
        TextView videofenlei = ((TextView) ret.findViewById(R.id.video_fenlei));
        TextView videotitle = ((TextView) ret.findViewById(R.id.video_title));
        ImageView usericon = ((ImageView) ret.findViewById(R.id.user_icon));
        TextView dianzan = (TextView) ret.findViewById(R.id.video_dianzan_count);
        TextView cai = (TextView) ret.findViewById(R.id.video_cai_count);
        TextView reply = (TextView)ret. findViewById(R.id.video_reply_count);
        TextView sharecount = (TextView)ret. findViewById(R.id.video_share_count);
        Cover = (ImageView)ret.findViewById(R.id.video_cover);
        username.setText(fdata.getUser().getName());
        videotitle.setText(fdata.getTitle());
        videofenlei.setText(fdata.getCategory_name());
        dianzan.setText(String.valueOf(fdata.getDigg_count()));
        cai.setText(String.valueOf(fdata.getBury_count()));
        reply.setText(String.valueOf(fdata.getRepin_count()));
        sharecount.setText(String.valueOf(fdata.getShare_count()));
        Picasso.with(getContext()).load(fdata.getCover().getUrl_list().get(0).getUrl()).into(Cover);
        Picasso.with(getContext()).load(fdata.getUser().getIconUrl()).into(usericon);
        //发送评论的
        GroupId groupId = new GroupId();
        groupId.setGroup_id(fdata.getGroup_id());
        EventBus.getDefault().post(groupId);
        sharecount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(fdata);
            }
        });
        
        btnplay = (ImageView) ret.findViewById(R.id.btn_play);
        btnplay.setOnClickListener(this);

        mUrl = fdata.getMp4_url();
        mSurfaceView = (SurfaceView) ret.findViewById(R.id.video_play);
        SurfaceHolder holder = mSurfaceView.getHolder();
        holder.addCallback(this);
        mSurfaceView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPlaying) {
                    isPlaying = false;
                    sMediaPlayer.pause();
                    btnplay.setVisibility(View.VISIBLE);
                }else {
                    isPlaying = true;
                    sMediaPlayer.start();
                    btnplay.setVisibility(View.INVISIBLE)
                    ;
                }
            }
        });

        sMediaPlayer = new MediaPlayer();
        sMediaPlayer.setOnCompletionListener(this);
        sMediaPlayer.setOnPreparedListener(this);
        return ret;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
    }

    @Override
    public void onDestroy() {
        if (sMediaPlayer != null) {
            sMediaPlayer.stop();
            sMediaPlayer.reset();
            sMediaPlayer.release();
            sMediaPlayer = null;
        }
        super.onDestroy();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        sMediaPlayer.setDisplay(holder);
        if (mUrl != null) {
            try {
                Log.d("aaaaaa", "surfaceCreated: " + mUrl);
                sMediaPlayer.setDataSource(mUrl);
                sMediaPlayer.prepareAsync();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (sMediaPlayer != null) {
            sMediaPlayer.stop();
            sMediaPlayer.reset();
            sMediaPlayer.release();
            sMediaPlayer = null;
        }
    }

    @Override
    public void onClick(View v) {
        if (!isPlaying) {
            isPlaying = true;
            btnplay.setVisibility(View.INVISIBLE);
            Cover.setVisibility(View.INVISIBLE);
            sMediaPlayer.start();
        }else {
            isPlaying = false;
            btnplay.setVisibility(View.VISIBLE);
            sMediaPlayer.pause();
        }
        
    }
}
