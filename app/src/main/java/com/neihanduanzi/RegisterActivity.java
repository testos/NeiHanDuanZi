package com.neihanduanzi;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText userName;
    private EditText userPass;
    private Button register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        userName = (EditText) findViewById(R.id.user_name);
        userPass = (EditText) findViewById(R.id.user_pass);
        register = (Button) findViewById(R.id.register_view);

        register.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        SharedPreferences preferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();

        String name = userName.getText().toString().trim();
        String passwd = userPass.getText().toString().trim();

        edit.putString("userName", name);
        edit.putString("userPass", passwd);

        edit.commit();

        Toast.makeText(this, "注册成功，请登录", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, EnterActivity.class);
        startActivity(intent);
        finish();
    }
}
