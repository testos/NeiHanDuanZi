package com.neihanduanzi;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.neihanduanzi.model.FoundModel1;
import com.squareup.picasso.Picasso;

public class FoundDetailActivity extends AppCompatActivity {

    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_found_detail);
        mToolbar = (Toolbar) findViewById(R.id.found_detail_toolbar);
        WebView webView = (WebView) findViewById(R.id.found_detail_web);
        TextView txtTitle = (TextView) findViewById(R.id.found_detail_title);
        ImageView imageView = (ImageView) findViewById(R.id.found_detail_img);
        TextView txtIntro = (TextView) findViewById(R.id.found_detail_introduce);
        TextView txtCount = (TextView) findViewById(R.id.found_detail_count);
        setSupportActionBar(mToolbar);

        Intent intent = getIntent();
        String share_url = intent.getStringExtra("url");
        String title = intent.getStringExtra("name");
        String imgUrl = intent.getStringExtra("imgUrl");
        String intro = intent.getStringExtra("intro");
        int count = intent.getIntExtra("count", 0);
        int updates = intent.getIntExtra("updates", 0);
        String s1 = Integer.toString(count);
        String s2 = Integer.toString(updates);

        txtTitle.setText(title);
        String s =  "<font color='#e16767'>" + s1 + "</font>" + "人参与 | 总贴数"
                + "<font color='#e16767'>" + s2 + "</font>";
        txtCount.setText(Html.fromHtml(s));
        txtIntro.setText(intro);
        Context context = imageView.getContext();
        Picasso.with(context)
                .load(imgUrl)
                .resize(100, 100)
                .config(Bitmap.Config.ARGB_8888)
                .into(imageView);
        if (!share_url.isEmpty() && share_url.length() > 0){
            webView.loadUrl(share_url);
        }

    }
}
