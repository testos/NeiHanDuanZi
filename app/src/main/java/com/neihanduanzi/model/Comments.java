package com.neihanduanzi.model;

/**
 * Created by 凸显 on 2016/10/30.
 */

import com.google.gson.annotations.SerializedName;

/**
 * 评论的实体类
 */
public class Comments {
    @SerializedName("id")
    private long id;
    @SerializedName("text")
    private String content;
    @SerializedName("user_name")
    private String userName;
    @SerializedName("create_time")
    private long createTime;
    //用户头像
    @SerializedName("avatar_url")
    private String imgUrl;
    //点赞数量
    @SerializedName("digg_count")
    private int diggCount;
    //分享页面
    @SerializedName("share_url")
    private String shareUrl;


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public int getDiggCount() {
        return diggCount;
    }

    public void setDiggCount(int diggCount) {
        this.diggCount = diggCount;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getShareUrl() {
        return shareUrl;
    }

    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }


    @Override
    public String toString() {
        return "Comments{" +
                "content='" + content + '\'' +
                ", id=" + id +
                ", userName='" + userName + '\'' +
                ", createTime=" + createTime +
                ", imgUrl='" + imgUrl + '\'' +
                ", diggCount=" + diggCount +
                ", shareUrl='" + shareUrl + '\'' +
                '}';
    }
}
