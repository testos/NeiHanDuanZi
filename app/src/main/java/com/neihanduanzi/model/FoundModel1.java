package com.neihanduanzi.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Administrator on 2016/10/31 0031.
 */

public class FoundModel1 {


    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private Data data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        @SerializedName("categories")
        private Categories categories;
        /**
         * count : 1
         * banners : [{"target_users":[],"schema_url":"detail:51823651782","banner_url":{"title":"天蝎宝宝嗨起来~","url_list":[{"url":"http://p9.pstatp.com/origin/ef400087b7d7fbdec85"},{"url":"http://pb3.pstatp.com/origin/ef400087b7d7fbdec85"},{"url":"http://pb3.pstatp.com/origin/ef400087b7d7fbdec85"}],"uri":"ef400087b7d7fbdec85","height":248,"width":640,"id":375}}]
         */

        @SerializedName("rotate_banner")
        private RotateBanner rotate_banner;


        public Categories getCategories() {
            return categories;
        }

        public void setCategories(Categories categories) {
            this.categories = categories;
        }

        public RotateBanner getRotate_banner() {
            return rotate_banner;
        }

        public void setRotate_banner(RotateBanner rotate_banner) {
            this.rotate_banner = rotate_banner;
        }

        public static class Categories {
            @SerializedName("name")
            private String name;
            @SerializedName("intro")
            private String intro;
            @SerializedName("category_list")
            private List<CategoryList> category_list;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getIntro() {
                return intro;
            }

            public void setIntro(String intro) {
                this.intro = intro;
            }

            public List<CategoryList> getCategory_list() {
                return category_list;
            }

            public void setCategory_list(List<CategoryList> category_list) {
                this.category_list = category_list;
            }

            public static class CategoryList {
                @SerializedName("intro")
                private String intro;
                @SerializedName("id")
                private int id;
                @SerializedName("icon_url")
                private String icon_url;
                @SerializedName("share_url")
                private String share_url;
                @SerializedName("type")
                private int type;
                @SerializedName("today_updates")
                private int today_updates;
                @SerializedName("total_updates")
                private int total_updates;
                @SerializedName("subscribe_count")
                private int subscribe_count;
                @SerializedName("name")
                private String name;
                @SerializedName("tag")
                private String tag;
                @SerializedName("small_icon_url")
                private String small_icon_url;



                public String getIntro() {
                    return intro;
                }

                public void setIntro(String intro) {
                    this.intro = intro;
                }

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getIcon_url() {
                    return icon_url;
                }

                public void setIcon_url(String icon_url) {
                    this.icon_url = icon_url;
                }

                public String getShare_url() {
                    return share_url;
                }

                public void setShare_url(String share_url) {
                    this.share_url = share_url;
                }

                public int getType() {
                    return type;
                }

                public void setType(int type) {
                    this.type = type;
                }

                public int getToday_updates() {
                    return today_updates;
                }

                public void setToday_updates(int today_updates) {
                    this.today_updates = today_updates;
                }





                public int getTotal_updates() {
                    return total_updates;
                }

                public void setTotal_updates(int total_updates) {
                    this.total_updates = total_updates;
                }

                public int getSubscribe_count() {
                    return subscribe_count;
                }

                public void setSubscribe_count(int subscribe_count) {
                    this.subscribe_count = subscribe_count;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getTag() {
                    return tag;
                }

                public void setTag(String tag) {
                    this.tag = tag;
                }

                public String getSmall_icon_url() {
                    return small_icon_url;
                }

                public void setSmall_icon_url(String small_icon_url) {
                    this.small_icon_url = small_icon_url;
                }


            }
        }

        public static class RotateBanner {
            /**
             * target_users : []
             * schema_url : detail:51823651782
             * banner_url : {"title":"天蝎宝宝嗨起来~","url_list":[{"url":"http://p9.pstatp.com/origin/ef400087b7d7fbdec85"},{"url":"http://pb3.pstatp.com/origin/ef400087b7d7fbdec85"},{"url":"http://pb3.pstatp.com/origin/ef400087b7d7fbdec85"}],"uri":"ef400087b7d7fbdec85","height":248,"width":640,"id":375}
             */

            private List<Banners> banners;



            public List<Banners> getBanners() {
                return banners;
            }

            public void setBanners(List<Banners> banners) {
                this.banners = banners;
            }

            public static class Banners {

                /**
                 * title : 天蝎宝宝嗨起来~
                 * url_list : [{"url":"http://p9.pstatp.com/origin/ef400087b7d7fbdec85"},{"url":"http://pb3.pstatp.com/origin/ef400087b7d7fbdec85"},{"url":"http://pb3.pstatp.com/origin/ef400087b7d7fbdec85"}]
                 * uri : ef400087b7d7fbdec85
                 * height : 248
                 * width : 640
                 * id : 375
                 */

                @SerializedName("banner_url")
                private BannerUrl banner_url;

                public BannerUrl getBanner_url() {
                    return banner_url;
                }

                public void setBanner_url(BannerUrl banner_url) {
                    this.banner_url = banner_url;
                }

                public static class BannerUrl {
                    @SerializedName("title")
                    private String title;


                    /**
                     * url : http://p9.pstatp.com/origin/ef400087b7d7fbdec85
                     */

                    @SerializedName("url_list")
                    private List<UrlListBean> url_list;

                    public String getTitle() {
                        return title;
                    }

                    public void setTitle(String title) {
                        this.title = title;
                    }



                    public List<UrlListBean> getUrl_list() {
                        return url_list;
                    }

                    public void setUrl_list(List<UrlListBean> url_list) {
                        this.url_list = url_list;
                    }

                    public static class UrlListBean {
                        private String url;

                        public String getUrl() {
                            return url;
                        }

                        public void setUrl(String url) {
                            this.url = url;
                        }
                    }
                }
            }
        }
    }
}
